## Usage
To run the demo, open the project in Eclipse and run the `webserver.Webserver` class, then navigate to [http://localhost:7000](http://localhost:7000) in a web browser. (There is probably an equivalent Maven command but I'm not familiar with `mvn`.)

## Pre-proposal
Available [here](https://docs.google.com/document/d/11KkX6VpCTFxazwk6WFRu9h67huDIfE8BU_8c8XuodyY/edit?usp=sharing).

## Proposal
Available [here](https://docs.google.com/document/d/12gnOe0VYPcWpP9IxvU0IpjKpETYq8avcoI4JMtOhq24/edit?usp=sharing).

## Design document
Available [here](https://docs.google.com/document/d/1jk_c99d4Fs_wCueF2pVX_bhm_Z6jdwcpGMz_Ry6CauM/edit?usp=sharing).

## Project report
Available [here](https://docs.google.com/document/d/1toYSNszv7r0ur0yvQDB66NzPYfmstObgLc_yvtPLv4g/edit?usp=sharing).
