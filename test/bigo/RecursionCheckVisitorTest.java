package bigo;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ast.java8.Parser;

class RecursionCheckVisitorTest {

    private RecursionCheckVisitor visitor = new RecursionCheckVisitor();

    @Test
    void testNotRecursive() {
        assertFalse(Parser.parseMethod("void f() { return; }").accept(visitor));
        assertFalse(Parser.parseMethod("int f() { return 1; }").accept(visitor));
        assertFalse(Parser.parseMethod("int f(int a) { return a * a; }").accept(visitor));
        assertFalse(Parser.parseMethod("int f(int b) { if (b > 2) return 20; else return 10; }").accept(visitor));
        assertFalse(Parser.parseMethod("int f(int n) { int sum = 0; for (int i = 0; i < n; i++) sum += i; return sum; }").accept(visitor));
        assertFalse(Parser.parseMethod("void f(int n) { for (int i = 0; i < n; i += 1) System.out.println(i); }").accept(visitor));
    }

    @Test
    void testRecursive() {
        assertTrue(Parser.parseMethod("int f(int n) { if (n == 1) return 1; return n + f(n - 1); }").accept(visitor));
        assertTrue(Parser.parseMethod("int f(int n) { if (n == 1) return 1; return n + f(n / 2); }").accept(visitor));
    }

}
