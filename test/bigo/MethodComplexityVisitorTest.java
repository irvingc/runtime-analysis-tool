package bigo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collections;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ast.java8.Parser;
import ast.math.Addition;
import ast.math.Constant;
import ast.math.Variable;
import ast.simple.AstNode;

class MethodComplexityVisitorTest {

    private static final EquationDisplayPrinter PRINTER = EquationDisplayPrinter.INSTANCE;

    private MethodComplexityVisitor visitor;

    @BeforeAll
    static void setUpBeforeClass() throws Exception {
    }

    @AfterAll
    static void tearDownAfterClass() throws Exception {
    }

    @BeforeEach
    void setUp() throws Exception {
        visitor = new MethodComplexityVisitor(Collections.emptyMap());
    }

    @AfterEach
    void tearDown() throws Exception {
    }

    @Test
    void testVoid() {
        AstNode n = Parser.parseMethod("void f() { int a = 1 + 2; }");

        Complexity c = visitor.visit(n).simplify();

        assertTrue(c.runtime instanceof Constant);
        assertNull(c.output);
    }

    @Test
    void testConstantInt() {
        AstNode n = Parser.parseMethod("int f() { return 1; }");

        Complexity c = visitor.visit(n).simplify();

        assertTrue(c.runtime instanceof Constant);
        assertEquals(Constant.ONE, c.output);
    }

    @Test
    void testConstantAddition() {
        AstNode n = Parser.parseMethod("int f() { return 1 + 2; }");

        Complexity c = visitor.visit(n).simplify();

        assertTrue(c.runtime instanceof Constant);
        assertEquals(new Constant(3), c.output);
    }

    @Test
    void testIdentity() {
        AstNode n = Parser.parseMethod("int f(int n) { return n; }");

        Complexity c = visitor.visit(n).simplify();

        assertTrue(c.runtime instanceof Constant);
        assertEquals(new Variable("n"), c.output);
    }

    @Test
    void testVariableConstantAddition() {
        AstNode n = Parser.parseMethod("int f(int n) { return n + 1; }");

        Complexity c = visitor.visit(n).simplify();

        assertTrue(c.runtime instanceof Constant);
        assertTrue(c.output instanceof Addition);
        Addition addition = (Addition) c.output;
        assertEquals(2, addition.terms.size());
        assertEquals(new Variable("n"), addition.terms.get(0));
        assertEquals(Constant.ONE, addition.terms.get(1));
    }

    @Test
    void testArgumentAddition() {
        AstNode n = Parser.parseMethod("int f(int a, int b) { return a + b; }");

        Complexity c = visitor.visit(n).simplify();

        assertTrue(c.runtime instanceof Constant);
        assertTrue(c.output instanceof Addition);
        Addition addition = (Addition) c.output;
        assertEquals(2, addition.terms.size());
        assertEquals(new Variable("a"), addition.terms.get(0));
        assertEquals(new Variable("b"), addition.terms.get(1));
    }

    @Test
    void testArraySum() {
        AstNode n = Parser.parseMethod("int f(int[] arr) { int sum = 0; for (int n : arr) { sum = sum + n; } return sum; }");

        Complexity c = visitor.visit(n).simplify();

        assertEquals("((arr.length * 2) + 5)", PRINTER.visit(c.runtime));
    }

    @Test
    void testGaussSum() {
        AstNode n = Parser.parseMethod("int f(int n) { if (n == 1) return 1; else return n + f(n - 1); }");

        Complexity c = visitor.visit(n).simplify();

        assertEquals("f(n) = 6, f(1) = 1", PRINTER.visit(c.runtime));
        assertEquals("f(n) = (n + f((n-1))), f(1) = 1", PRINTER.visit(c.output));
    }

    @Test
    void testGaussSumIterative() {
        AstNode n = Parser.parseMethod("int f(int n) { int sum = 0; for (int i = 1; i <= n; i = i + 1) { sum = sum + i; } return sum; }");

        Complexity c = visitor.visit(n).simplify();

        assertEquals("(((n-1) * 2) + 4)", PRINTER.visit(c.runtime));
    }

}
