package main;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.ParserRuleContext;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.logging.LogFactory;

import ast.math.MathExpression;
import ast.math.Variable;
import ast.simple.AstNode;
import ast.simple.ClassDecl;
import bigo.BigOAnalysis;
import bigo.EquationSimplifier;
import bigo.SimpleGrammarPrettyPrint;
import grammar.Java8Lexer;
import grammar.Java8Parser;
import visitors.PrettyPrintVisitor;
import visitors.SimplifierVisitor;
import wolfram.WolframQuery;

public class Main {

    private static final org.apache.commons.logging.Log log = LogFactory.getLog(Main.class);

    public static void main(String[] args) {
        args = new String[] { "sample/UseOutputSimple.java" };
        List<String> inputFiles = new ArrayList<>();
        try {
            if (args.length > 0) {
                // for each directory/file specified on the command line
                for (int i = 0; i < args.length; i++) {
                    inputFiles.add(args[i]);
                }
                List<String> javaFiles = new ArrayList<>();
                for (String fileName : inputFiles) {
                    List<String> files = getFilenames(new File(fileName));
                    javaFiles.addAll(files);
                }
                doFiles(javaFiles);
            } else {
                System.err.println("Usage: java Main <directory or file name>");
            }
        } catch (Exception e) {
            System.err.println("exception: " + e);
            e.printStackTrace(System.err);   // so we can get stack trace
        }
    }

    public static void doFiles(List<String> files) throws Exception {
        long parserStart = System.currentTimeMillis();
        for (String f : files) {
            parseFile(f);
        }
        long parserStop = System.currentTimeMillis();
        System.out.println("Total lexer+parser time " + (parserStop - parserStart) + "ms.");
    }


    public static List<String> getFilenames(File f) throws Exception {
        List<String> files = new ArrayList<String>();
        getFilenames_(f, files);
        return files;
    }

    public static void getFilenames_(File f, List<String> files) throws Exception {
        // If this is a directory, walk each file/dir in that directory
        if (f.isDirectory()) {
            String flist[] = f.list();
            for (int i = 0; i < flist.length; i++) {
                getFilenames_(new File(f, flist[i]), files);
            }
        }

        // otherwise, if this is a java file, parse it!
        else if (((f.getName().length() > 5) &&
                f.getName().substring(f.getName().length() - 5).equals(".java"))) {
            files.add(f.getAbsolutePath());
        }
    }

    public static WolframEquation parseEquation(String simplifiedEquation) {
        String finalEquation = null;
        String error = null;
        try {
            WolframQuery wolf = new WolframQuery();
            String solved = wolf.getWolframPlaintext(simplifiedEquation);
            finalEquation = solved == null ? simplifiedEquation : solved;
//            finalEquation = finalEquation.replace(" ", "\\cdot ");
            log.info("Final: " + finalEquation);
        } catch (Exception e) {
            error = e.getMessage();
            e.printStackTrace();
        }
        return new WolframEquation(finalEquation, error);
    }

    public static Result parsePartial(String text) {
        String antlrAst = null;
        String simplifiedAst = null;
        String runtimeAst = null;
        String outputAst = null;
        String assumptions = null;
        String error = null;

        try {
            Lexer lexer = new Java8Lexer(new ANTLRInputStream(text));
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            Java8Parser parser = new Java8Parser(tokens);

            // Parse
            ParserRuleContext t = parser.compilationUnit();

            // Generate ANTLR AST
            antlrAst = new PrettyPrintVisitor(parser).visit(t);

            // Generate simple AST
            ClassDecl simpleAst = (ClassDecl) new SimplifierVisitor(parser).visit(t);
            simplifiedAst = new SimpleGrammarPrettyPrint().visit(simpleAst);
            log.info(simplifiedAst);

            // Generate equations (math AST)
            BigOAnalysis b = BigOAnalysis.of((ClassDecl) simpleAst);
            String methodName = simpleAst.methods.get(simpleAst.methods.size() - 1).getName();
            MathExpression runtime = b.getRuntimeComplexity(methodName);
            Set<Variable> variables = runtime.getFreeVariables();
            Map<Variable, MathExpression> bindings = new HashMap<>();
            int i = 0;
            for (Variable v : variables) {
                String newVar = "N" + (i++);
                Variable v2 = new Variable(newVar);
                bindings.put(v, v2);
//                assumptions.put(newVar, v.name);
            }
            assumptions = bindings.toString();

            runtimeAst = runtime.bindVariables(bindings).toEquation();
            log.info(runtimeAst);

//            runtimeAst = b.getRuntimeComplexity(methodName).toEquation();
//            outputAst = b.getOutputComplexity(methodName).toEquation();
        } catch (Exception e) {
            System.err.println("Hit error");
            error = ExceptionUtils.getStackTrace(e);
            e.printStackTrace();
        }

        return new Result(
                antlrAst,
                simplifiedAst,
                runtimeAst,
                outputAst,
                assumptions,
                null,
                null,
                error);

    }

    public static Result parseFull(String text) {
        Result result = parsePartial(text);
        if (result.error != null) {
            return result;
        }

        WolframEquation eq;
        String runtimeResult = null, outputResult = null, error = null;

        eq = parseEquation(result.runtimeAst);
        if (eq.getError() == null) {
            runtimeResult = eq.getFinalEquation();

            if (result.outputAst != null) {
                eq = parseEquation(result.outputAst);

                if (eq.getError() == null) {
                    outputResult = eq.getFinalEquation();
                } else {
                    error = eq.getError();
                }
            }
        } else {
            error = eq.getError();
        }

        return new Result(
                result.antlrAst,
                result.simplifiedAst,
                result.runtimeAst,
                result.outputAst,
                result.assumptions,
                runtimeResult,
                outputResult,
                error);
    }

    public static void parseFile(String f) {
        try {
//            System.err.println(f);

            // Setup
            Lexer lexer = new Java8Lexer(new ANTLRFileStream(f));
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            Java8Parser parser = new Java8Parser(tokens);

            // Parse
            ParserRuleContext t = parser.compilationUnit();
//            System.out.println(parser.getRuleNames()[t.getRuleIndex()]);

            // Generate ANTLR AST
//            System.out.println(t.toStringTree(parser));
//            System.out.println(new PrettyPrintVisitor(parser).visit(t));

            // Generate Simple AST
            AstNode simpleAst = new SimplifierVisitor(parser).visit(t);
            System.out.println(new SimpleGrammarPrettyPrint().visit(simpleAst));

            // Generate raw equation
//            BigOVisitor b = new BigOVisitor();
//            b.visit(simpleAst);
            BigOAnalysis b = BigOAnalysis.of((ClassDecl) simpleAst);
//            System.out.println(b.getMethodRuntimeEquation("square"));
//            System.out.println(b.getMethodRuntimeEquation("func1"));
//            MathExpression rawEq = b.visit(simpleAst);
            MathExpression rawEq = b.getRuntimeComplexity("func1");
            System.out.println("Raw equation: " + rawEq.toEquation());

            // Generate simplified equation
            String simplified = new EquationSimplifier().visit(rawEq).toEquation();
            System.out.println("Simplified equation: " + simplified);

//            // Call wolfram
//            WolframQuery wolf = new WolframQuery();
//            String solved = wolf.getWolframPlaintext(simplified);
//            System.out.println("Solved: " + solved);
//            solved = solved == null ? simplified : solved;
//            System.out.println("Final: " + solved);
//
//            // Print assumptions
//            System.out.println("Assumptions: " + b.getAssumptions());
        } catch (Exception e) {
            System.err.println("parser exception: " + e);
            e.printStackTrace();   // so we can get stack trace
        }
    }
}