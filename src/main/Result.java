package main;

public class Result {

    public final String antlrAst;
    public final String simplifiedAst;
    public final String runtimeAst;
    public final String outputAst;
    public final String assumptions;
    public final String runtimeResult;
    public final String outputResult;

    public final String error;

    public Result(String antlrAst,
                  String simplifedAst,
                  String runtimeAst,
                  String outputAst,
                  String assumptions,
                  String runtimeResult,
                  String outputResult,
                  String error) {
        this.antlrAst = antlrAst;
        this.simplifiedAst = simplifedAst;
        this.runtimeAst = runtimeAst;
        this.outputAst = outputAst;
        this.assumptions = assumptions;
        this.runtimeResult = runtimeResult;
        this.outputResult = outputResult;
        this.error = error;
    }

}
