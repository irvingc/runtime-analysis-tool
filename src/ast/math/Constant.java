package ast.math;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

public class Constant implements MathExpression {

    public static final Constant ZERO = new Constant(0);
    public static final Constant ONE = new Constant(1);

    public final int value;

    public Constant(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

    @Override
    public String toEquation() {
        return String.valueOf(value);
    }

    @Override
    public <T> T accept(MathExpressionVisitor<T> visitor) {
        return visitor.visitConstant(this);
    }

    @Override
    public Set<Variable> getFreeVariables() {
        return Collections.emptySet();
    }

    @Override
    public MathExpression bindVariables(Map<Variable, MathExpression> bindings) {
        return this;
    }

    @Override
    public int hashCode() {
        return value;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Constant other = (Constant) obj;
        if (value != other.value)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Constant[" + value + "]";
    }

}
