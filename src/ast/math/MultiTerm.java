package ast.math;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.StringJoiner;
import java.util.stream.Collectors;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

public abstract class MultiTerm implements MathExpression {

    public final ImmutableList<MathExpression> terms;

    public MultiTerm(MathExpression ... terms) {
        this(Arrays.asList(terms));
    }

    public MultiTerm(List<MathExpression> terms) {
        Preconditions.checkArgument(!terms.isEmpty());
        this.terms = ImmutableList.copyOf(terms);
    }

    public ImmutableList<MathExpression> getTerms() {
        return this.terms;
    }

    protected abstract String joinString();

    @Override
    public String toEquation() {
        StringJoiner output = new StringJoiner(joinString(), "(", ")");
        for (MathExpression e : terms) {
            output.add(e.toEquation());
        }
        return output.toString();
    }

    @Override
    public Set<Variable> getFreeVariables() {
        return terms.stream().flatMap(subexpr -> subexpr.getFreeVariables().stream()).collect(Collectors.toSet());
    }

}
