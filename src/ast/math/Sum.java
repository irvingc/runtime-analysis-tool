package ast.math;

import java.util.Map;
import java.util.Set;

import com.google.common.base.Preconditions;

public class Sum implements MathExpression {

    private final MathExpression start;
    private final MathExpression end;
    private final MathExpression body;
    private final Variable index;

    public Sum(MathExpression start, MathExpression end, MathExpression body, Variable index) {
        Preconditions.checkNotNull(start);
        Preconditions.checkNotNull(end);
        Preconditions.checkNotNull(body);
        Preconditions.checkNotNull(index);
        this.start = start;
        this.end = end;
        this.body = body;
        this.index = index;
    }

    public Variable getIndex() {
        return index;
    }

    public MathExpression getStart() {
        return this.start;
    }

    public MathExpression getEnd() {
        return this.end;
    }

    public MathExpression getBody() {
        return this.body;
    }

    @Override
    public String nodeName() {
        return "sum";
    }

    @Override
    public String toEquation() {
        return String.format("(sum from %s=%s to %s of %s)",
                this.index.toEquation(),
                removeExtraParens(this.start.toEquation()),
                removeExtraParens(this.end.toEquation()),
                this.body.toEquation());
    }

    @Override
    public <T> T accept(MathExpressionVisitor<T> visitor) {
        return visitor.visitSum(this);
    }

    @Override
    public Set<Variable> getFreeVariables() {
        return body.getFreeVariables();
    }

    @Override
    public MathExpression bindVariables(Map<Variable, MathExpression> bindings) {
        return new Sum(start, end, body.bindVariables(bindings), index);
    }

}
