package ast.math;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Subtraction extends MultiTerm {

    public Subtraction(MathExpression ... terms) {
        super(terms);
    }

    public Subtraction(List<MathExpression> terms) {
        super(terms);
    }

    @Override
    public <T> T accept(MathExpressionVisitor<T> visitor) {
        return visitor.visitSubtraction(this);
    }

    @Override
    public MathExpression bindVariables(Map<Variable, MathExpression> bindings) {
        return new Subtraction(terms.stream().map(term -> term.bindVariables(bindings)).collect(Collectors.toList()));
    }

    @Override
    protected String joinString() {
        return " - ";
    }

}
