package ast.math;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface MathExpression {

    String toEquation();

    default String nodeName() {
        return getClass().getSimpleName();
    }

    default String removeExtraParens(String eq) {
        while (eq.startsWith("(") && eq.endsWith(")")) {
            eq = eq.substring(1, eq.length() - 1);
        }
        return eq;
    }

    default List<MathExpression> failOnEmpty(List<MathExpression> items) {
        if (items.size() == 0) {
            throw new IllegalArgumentException("Empty term");
        }
        return items;
    }

    <T> T accept(MathExpressionVisitor<T> visitor);

    Set<Variable> getFreeVariables();

    MathExpression bindVariables(Map<Variable, MathExpression> bindings);

}
