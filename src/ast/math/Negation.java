package ast.math;

import java.util.Arrays;
import java.util.Map;
import java.util.Set;

public class Negation implements MathExpression {

    private final MathExpression expression;
    private final MathExpression negatedExpression;

    public Negation(MathExpression expression) {
        this.expression = expression;
        if (expression.nodeName().equals("Constant")) {
            this.negatedExpression = new Constant(-1 * ((Constant) expression).getValue());
        } else {
            this.negatedExpression = new Subtraction(Arrays.asList(new Constant(0), this.expression));
        }
    }

    public MathExpression getValue() {
        return this.negatedExpression;
    }

    public MathExpression getOriginalValue() {
        return this.expression;
    }

    @Override
    public String toEquation() {
        return "-" + this.expression.toEquation();
    }

    @Override
    public <T> T accept(MathExpressionVisitor<T> visitor) {
        return visitor.visitNegation(this);
    }

    @Override
    public Set<Variable> getFreeVariables() {
        return expression.getFreeVariables();
    }

    @Override
    public MathExpression bindVariables(Map<Variable, MathExpression> bindings) {
        return new Negation(expression.bindVariables(bindings));
    }

}
