package ast.math;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Division extends MultiTerm {

    public Division(MathExpression ... terms) {
        super(terms);
    }

    public Division(List<MathExpression> terms) {
        super(terms);
    }

    @Override
    public <T> T accept(MathExpressionVisitor<T> visitor) {
        return visitor.visitDivision(this);
    }

    @Override
    public MathExpression bindVariables(Map<Variable, MathExpression> bindings) {
        return new Division(terms.stream().map(term -> term.bindVariables(bindings)).collect(Collectors.toList()));
    }

    @Override
    protected String joinString() {
        return " / ";
    }

}
