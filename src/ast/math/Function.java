package ast.math;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;

public class Function implements MathExpression {

    public final String name;
    public final ImmutableList<MathExpression> arguments;

    public Function(String name, List<MathExpression> arguments) {
        this.name = name;
        this.arguments = ImmutableList.copyOf(arguments);
    }

    @Override
    public String toEquation() {
        return String.format("%s(%s)", name, String.join(", ", arguments.stream().map(e -> e.toEquation()).collect(Collectors.toList())));
    }

    @Override
    public <T> T accept(MathExpressionVisitor<T> visitor) {
        return visitor.visitFunction(this);
    }

    @Override
    public Set<Variable> getFreeVariables() {
        return arguments.stream().flatMap(subexpr -> subexpr.getFreeVariables().stream()).collect(Collectors.toSet());
    }

    @Override
    public MathExpression bindVariables(Map<Variable, MathExpression> bindings) {
        return new Function(name, arguments.stream().map(term -> term.bindVariables(bindings)).collect(Collectors.toList()));
    }
    
}
