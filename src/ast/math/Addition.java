package ast.math;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Addition extends MultiTerm {

    public Addition(MathExpression ... terms) {
        super(terms);
    }

    public Addition(List<MathExpression> terms) {
        super(terms);
    }

    @Override
    public String nodeName() {
        return "+";
    }

    @Override
    public <T> T accept(MathExpressionVisitor<T> visitor) {
        return visitor.visitAddition(this);
    }

    @Override
    public MathExpression bindVariables(Map<Variable, MathExpression> bindings) {
        return new Addition(terms.stream().map(term -> term.bindVariables(bindings)).collect(Collectors.toList()));
    }

    @Override
    protected String joinString() {
        return " + ";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Addition other = (Addition) obj;
        if (terms == null) {
            if (other.terms != null)
                return false;
        } else if (!terms.equals(other.terms))
            return false;
        return true;
    }

}
