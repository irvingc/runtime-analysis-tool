package ast.math;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Multiplication extends MultiTerm {

    public Multiplication(MathExpression ... terms) {
        super(terms);
    }

    public Multiplication(List<MathExpression> terms) {
        super(terms);
    }

    @Override
    public <T> T accept(MathExpressionVisitor<T> visitor) {
        return visitor.visitMultiplication(this);
    }

    @Override
    public MathExpression bindVariables(Map<Variable, MathExpression> bindings) {
        return new Multiplication(terms.stream().map(term -> term.bindVariables(bindings)).collect(Collectors.toList()));
    }

    @Override
    protected String joinString() {
        return " * ";
    }

}
