package ast.math;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

import com.google.common.base.Preconditions;

public class Variable implements MathExpression, Comparable<Variable> {

    public final String name;

    public Variable(String name) {
        Preconditions.checkNotNull(name);
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public String toEquation() {
        return name;
    }

    @Override
    public <T> T accept(MathExpressionVisitor<T> visitor) {
        return visitor.visitVariable(this);
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Variable other = (Variable) obj;
        if (!name.equals(other.name))
            return false;
        return true;
    }

    @Override
    public Set<Variable> getFreeVariables() {
        return Collections.singleton(this);
    }

    @Override
    public MathExpression bindVariables(Map<Variable, MathExpression> bindings) {
        MathExpression expr = bindings.get(this);
        if (expr != null) {
            return expr;
        }

        return this;
    }

    @Override
    public int compareTo(Variable o) {
        return name.compareTo(o.name);
    }

}
