package ast.math;

import java.util.Map;
import java.util.Set;

public class Log implements MathExpression {

    public final MathExpression argument;

    public Log(MathExpression expression) {
        this.argument = expression;
    }

    public MathExpression getExpression() {
        return this.argument;
    }

    @Override
    public String nodeName() {
        return "Log";
    }

    @Override
    public String toEquation() {
        return "log(" + argument.toEquation() + ")";
    }

    @Override
    public <T> T accept(MathExpressionVisitor<T> visitor) {
        return visitor.visitLog(this);
    }

    @Override
    public Set<Variable> getFreeVariables() {
        return argument.getFreeVariables();
    }

    @Override
    public MathExpression bindVariables(Map<Variable, MathExpression> bindings) {
        return new Log(argument.bindVariables(bindings));
    }

}
