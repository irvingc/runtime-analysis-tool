package ast.math;

public interface MathExpressionVisitor<T> {

    T visitAddition(Addition expr);
    T visitConstant(Constant expr);
    T visitDivision(Division expr);
    T visitLog(Log expr);
    T visitMultiplication(Multiplication expr);
    T visitNegation(Negation expr);
    T visitSubtraction(Subtraction expr);
    T visitVariable(Variable expr);
    T visitSum(Sum expr);
    T visitRecurrence(Recurrence expr);
    T visitFunction(Function expr);

    default T visit(MathExpression expr) {
        return expr.accept(this);
    }

}
