package ast.math;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;

import com.google.common.collect.Sets;

public class Recurrence implements MathExpression {

    public final Variable function;
    public final Map<Variable, Constant> baseConditions;
    public final MathExpression recursiveCase;
    public final MathExpression baseCase;

    public Recurrence(Variable function, Map<Variable, Constant> baseConditions, MathExpression recursiveCase,
            MathExpression baseCase) {
        this.function = function;
        this.baseConditions = baseConditions;
        this.recursiveCase = recursiveCase;
        this.baseCase = baseCase;
    }

    public Variable getFunction() {
        return this.function;
    }

    public Set<Variable> getParameters() {
        return baseConditions.keySet();
    }

    public MathExpression getRecursiveCase() {
        return this.recursiveCase;
    }

    public MathExpression getBaseCase() {
        return this.baseCase;
    }

    @Override
    public String toEquation() {
        StringJoiner paramJoiner = new StringJoiner(", ", "(", ")");
        for (Variable var : baseConditions.keySet()) {
            paramJoiner.add(var.name);
        }

        StringJoiner zeroJoiner = new StringJoiner(", ", "(", ")");
        for (Variable var : baseConditions.keySet()) {
            zeroJoiner.add(baseConditions.get(var).toEquation());
        }

        return String.format("%s%s = %s, %s%s = %s",
                this.function.getName(),
                paramJoiner.toString(),
                this.recursiveCase.toEquation(),
                this.function.getName(),
                zeroJoiner.toString(),
                this.baseCase.toEquation());
    }

    @Override
    public <T> T accept(MathExpressionVisitor<T> visitor) {
        return visitor.visitRecurrence(this);
    }

    @Override
    public Set<Variable> getFreeVariables() {
        return Sets.union(baseCase.getFreeVariables(), recursiveCase.getFreeVariables());
    }

    @Override
    public MathExpression bindVariables(Map<Variable, MathExpression> bindings) {
        Map<Variable, Constant> baseConditions = new HashMap<>();
        for (Variable v : this.baseConditions.keySet()) {
            if (bindings.containsKey(v)) {
                baseConditions.put((Variable) bindings.get(v), this.baseConditions.get(v));
            } else {
                baseConditions.put(v, this.baseConditions.get(v));
            }
        }
        return new Recurrence(function, baseConditions, recursiveCase.bindVariables(bindings), baseCase.bindVariables(bindings));
    }

}
