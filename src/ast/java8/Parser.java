package ast.java8;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.ParserRuleContext;

import ast.simple.AstNode;
import grammar.Java8Lexer;
import grammar.Java8Parser;
import visitors.SimplifierVisitor;

public class Parser {

    public static AstNode parseMethod(String javaCode) {
        Lexer lexer = new Java8Lexer(new ANTLRInputStream(javaCode));
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        Java8Parser parser = new Java8Parser(tokens);
        ParserRuleContext t = parser.methodDeclaration();
        return new SimplifierVisitor(parser).visit(t);
    }

    public static AstNode parseClass(String javaCode) {
        Lexer lexer = new Java8Lexer(new ANTLRInputStream(javaCode));
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        Java8Parser parser = new Java8Parser(tokens);
        ParserRuleContext t = parser.compilationUnit();
        return new SimplifierVisitor(parser).visit(t);
    }

}
