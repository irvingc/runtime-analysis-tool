package ast.simple;

import java.util.List;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

public class ClassDecl implements AstNode {

    public final ImmutableList<MethodDecl> methods;
    public final String name;

    public ClassDecl(String name, List<MethodDecl> methods) {
        Preconditions.checkNotNull(name);
        Preconditions.checkNotNull(methods);
        Preconditions.checkArgument(!methods.isEmpty());

        this.name = name;
        this.methods = ImmutableList.copyOf(methods);
    }

    public String getClassName() {
        return this.name;
    }

    public List<MethodDecl> getMethods() {
        return this.methods;
    }

    public MethodDecl getMethod(String name) {
        for (MethodDecl method : methods) {
            if (method.getName().equals(name)) {
                return method;
            }
        }
        throw new IllegalArgumentException("Method not found");
    }

    @Override
    public String nodeName() {
        return "ClassDecl";
    }

    @Override
    public boolean isTerminal() {
        return false;
    }

    @Override
    public <T> T accept(AstNodeVisitor<T> visitor) {
        return visitor.visitClassDecl(this);
    }
}
