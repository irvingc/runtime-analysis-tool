package ast.simple;

import com.google.common.base.Preconditions;

public interface AstNode {

    String nodeName();
    boolean isTerminal();

    <T> T accept(AstNodeVisitor<T> visitor);

    default <T> T notNull(T item) {
        Preconditions.checkNotNull(item);
        return item;
    }

}
