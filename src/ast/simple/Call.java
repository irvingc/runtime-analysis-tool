package ast.simple;

import java.util.List;

public class Call implements AstNode {
    private String methodName;
    private List<AstNode> arguments;

    public Call(String methodName, List<AstNode> arguments) {
        this.methodName = notNull(methodName);
        this.arguments = notNull(arguments);
    }

    public String getMethodName() {
        return this.methodName;
    }

    public List<AstNode> getArguments() {
        return this.arguments;
    }

    @Override
    public String nodeName() {
        return "Call";
    }

    @Override
    public boolean isTerminal() {
        return false;
    }

    @Override
    public <T> T accept(AstNodeVisitor<T> visitor) {
        return visitor.visitCall(this);
    }
}
