package bigo;

import ast.math.MathExpression;

public class Complexity {

    public final MathExpression runtime;
    public final MathExpression output;

    public Complexity(MathExpression runtime, MathExpression output) {
        this.runtime = runtime;
        this.output = output;
    }

    public Complexity onlyRuntime() {
        return runtime(runtime);
    }

    public Complexity simplify() {
        EquationSimplifier simplifier = new EquationSimplifier();
        MathExpression runtime = this.runtime == null ? null : simplifier.visit(this.runtime);
        MathExpression output = this.output == null ? null : simplifier.visit(this.output);
        return Complexity.of(runtime, output);
    }

    public static Complexity runtime(MathExpression expr) {
        return new Complexity(expr, null);
    }

    public static Complexity of(MathExpression runtime, MathExpression output) {
        return new Complexity(runtime, output);
    }

}
