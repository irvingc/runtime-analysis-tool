package bigo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.stream.Collectors;

import org.apache.commons.logging.LogFactory;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;

import ast.math.Addition;
import ast.math.Constant;
import ast.math.Division;
import ast.math.Function;
import ast.math.MathExpression;
import ast.math.Multiplication;
import ast.math.Negation;
import ast.math.Recurrence;
import ast.math.Subtraction;
import ast.math.Sum;
import ast.math.Variable;
import ast.simple.Assignment;
import ast.simple.AstNode;
import ast.simple.AstNodeVisitor;
import ast.simple.BinOp;
import ast.simple.Call;
import ast.simple.ClassDecl;
import ast.simple.ForEachLoop;
import ast.simple.ForLoop;
import ast.simple.IfElse;
import ast.simple.Literal;
import ast.simple.Lookup;
import ast.simple.MethodDecl;
import ast.simple.MultipleAstNodes;
import ast.simple.Parameter;
import ast.simple.Return;
import ast.simple.SpecialCall;
import ast.simple.Type;
import ast.simple.UnaryOp;
import ast.simple.VariableDecl;

public class MethodComplexityVisitor extends AstNodeVisitor<Complexity> {

    private static final org.apache.commons.logging.Log log = LogFactory.getLog(MethodComplexityVisitor.class);

    private String currentMethodName;
    private Map<String, MathExpression> variables = new HashMap<>();
    private MathExpression returnValue;

    private Stack<Map<String, MathExpression>> variablesStack = new Stack<>();
    private Stack<MathExpression> returnValueStack = new Stack<>();
    private final ImmutableMap<String, FunctionCall> knownMethods;

    public MethodComplexityVisitor(Map<String, FunctionCall> knownMethods) {
        this.knownMethods = ImmutableMap.copyOf(knownMethods);
    }

    private void branch() {
        Map<String, MathExpression> variablesSave = variables;
        MathExpression returnValueSave = returnValue;

        variables = new HashMap<>(variablesSave);

        variablesStack.push(variablesSave);
        returnValueStack.push(returnValueSave);
    }

    private void unbranch() {
        variables = variablesStack.pop();
        returnValue = returnValueStack.pop();
    }

    @Override
    public Complexity visitClassDecl(ClassDecl node) {
        throw new UnsupportedOperationException();
    }

    private Complexity visitStatements(List<AstNode> statements) {
        List<MathExpression> terms = new ArrayList<>();
        for (AstNode statement : statements) {
            Complexity statementComplexity = visit(statement);
            terms.add(statementComplexity.runtime);
        }
        if (terms.isEmpty()) {
            log.warn("Statements have zero runtime");
        }
        return Complexity.runtime(terms.isEmpty() ? Constant.ZERO : new Addition(terms));
    }

    @Override
    public Complexity visitMethodDecl(MethodDecl node) {
        currentMethodName = node.getName();
        variables.clear();
        returnValue = null;

        List<AstNode> statements = node.getBody();
        Complexity bodyComplexity = visitStatements(statements);

//        Preconditions.checkState(node.getReturnType().getFullType().equals("void") || returnValue != null);
        MathExpression output = returnValue;

        if (RecursionCheckVisitor.isRecursive(node)) {
            Variable function = new Variable(currentMethodName);

            // TODO: Identify recursive parameter
            Map<Variable, Constant> baseConditions = new HashMap<>();
            for (Parameter p : node.getParameters()) {
                baseConditions.put(new Variable(p.getName()), Constant.ONE);
            }

            MathExpression baseCase = Constant.ONE;

            MathExpression runtime = new Recurrence(function, baseConditions, bodyComplexity.runtime, baseCase);
            if (output != null) {
                output = new Recurrence(function, baseConditions, output, baseCase);
            }
            return Complexity.of(runtime, output);
        } else {
            return Complexity.of(bodyComplexity.runtime, output);
        }
    }

    @Override
    public Complexity visitParameter(Parameter node) {
        throw new UnsupportedOperationException("Parameter declaration has no time or output complexity");
    }

    @Override
    public Complexity visitVariableDecl(VariableDecl node) {
        return Complexity.runtime(Constant.ONE);
    }

    @Override
    public Complexity visitAssignment(Assignment node) {
        Complexity valueComplexity = visit(node.getValue());
        variables.put(node.getName(), valueComplexity.output);
        return valueComplexity.onlyRuntime();
    }

    @Override
    public Complexity visitLookup(Lookup node) {
        final String name = node.getName();
        if (variables.containsKey(name)) {
            return Complexity.of(Constant.ONE, variables.get(node.getName()));
        } else {
            return Complexity.of(Constant.ONE, new Variable(name));
        }
    }

    @Override
    public Complexity visitForEachLoop(ForEachLoop node) {
        Complexity sequence = visit(node.getSequence());

        Preconditions.checkState(sequence.output instanceof Variable, "For-each loop only supported over lookup");

        Variable iterations = new Variable(((Variable)sequence.output).name + ".length");

        Complexity body = visitStatements(node.getBody());

        return Complexity.runtime(new Addition(sequence.runtime, new Multiplication(iterations, body.runtime)));
    }

    @Override
    public Complexity visitForLoop(ForLoop node) {
        // TODO: Make this better

        Preconditions.checkArgument(node.getCounter() instanceof Assignment, "For loop counter must be assignment");
        Assignment counter = (Assignment) node.getCounter();
        String loopVariable = counter.getName();

        Preconditions.checkArgument(node.getEnd() instanceof BinOp, "For loop condition must be a BinOp");
        BinOp end = (BinOp) node.getEnd();

        Preconditions.checkArgument(end.getOperator().equals("<") || end.getOperator().equals("<="),
                "For loop condition operator must be < or <=");
        Preconditions.checkArgument(end.getLeft() instanceof Lookup, "For loop variable must be on LHS of condition");

        Preconditions.checkArgument(node.getChange() instanceof Assignment, "For loop update must be assignment");
        Assignment change = (Assignment) node.getChange();

        Preconditions.checkArgument(change.getName().equals(loopVariable), "For loop update variable should be loop variable");
        Preconditions.checkArgument(change.getValue() instanceof BinOp, "For loop update value must be a BinOp");
        BinOp changeValue = (BinOp) change.getValue();

        Preconditions.checkArgument(changeValue.getLeft() instanceof Lookup, "For loop update value must be BinOp over loop variable and one");
        Preconditions.checkArgument(((Lookup)changeValue.getLeft()).getName().equals(loopVariable), "For loop update value must be BinOp over loop variable and one");

        Preconditions.checkArgument(changeValue.getRight() instanceof Literal, "For loop update value must be BinOp over loop variable and one");
        Preconditions.checkArgument(((Literal)changeValue.getRight()).getText().equals("1"), "For loop update value must be BinOp over loop variable and one");

        Complexity start = visit(counter.getValue());
        Complexity bound = visit(end.getRight());
        Complexity body = visitStatements(node.getBody());

        return Complexity.runtime(new Sum(start.output, bound.output, body.runtime, new Variable(loopVariable)));
    }

    @Override
    public Complexity visitReturn(Return node) {
        Complexity valueComplexity = visit(node.getValue());
        returnValue = valueComplexity.output;
        return valueComplexity.onlyRuntime();
    }

    @Override
    public Complexity visitIfElse(IfElse node) {
        Complexity condition = visit(node.getCondition());

        branch();
        Complexity trueBranch = visitStatements(node.getTrueBranch());
        MathExpression trueReturn = returnValue;
        unbranch();

        branch();
        Complexity falseBranch = visitStatements(node.getFalseBranch());
        MathExpression falseReturn = returnValue;
        unbranch();

//        Preconditions.checkState(trueReturn == falseReturn, "Can't analyze branching returns");
        log.warn("Can't accurately analyze branching returns");
        returnValue = falseReturn;

        // TODO: Don't oversimplify (e.g. take max, flow analysis, etc.)
        return Complexity.runtime(new Addition(condition.runtime, trueBranch.runtime, falseBranch.runtime));
    }

    @Override
    public Complexity visitSpecialCall(SpecialCall node) {
        Complexity object = visit(node.getObject());
        Variable lookup;
        Complexity index, value, size;

        // TODO: "Properties", array construct
        switch(node.getMethodName()) {
        case "length":
            Preconditions.checkState(object.output instanceof Variable, "Special call length only supported on lookups");
            lookup = (Variable) object.output;
            return Complexity.of(object.runtime, new Variable(lookup.name + ".length"));
        case "arrayget":
            Preconditions.checkState(object.output instanceof Variable, "Special call arrayget only supported on lookups");
            lookup = (Variable) object.output;
            Preconditions.checkArgument(node.getParameters().size() == 1);
            index = visit(node.getParameters().get(0));
            return Complexity.of(new Addition(object.runtime, index.runtime), new Variable(lookup.name + "[" + index.output.toEquation() + "]"));
        case "arrayput":
            Preconditions.checkState(object.output instanceof Variable, "Special call arrayput only supported on lookups");
            lookup = (Variable) object.output;
            Preconditions.checkArgument(node.getParameters().size() == 2);
            index = visit(node.getParameters().get(0));
            value = visit(node.getParameters().get(1));
            variables.put(lookup.name + "[" + index.output.toEquation() + "]", value.output);
            return Complexity.runtime(new Addition(object.runtime, index.runtime, value.runtime));
        case "arraycreation":
            Preconditions.checkArgument(node.getParameters().size() == 2);
            size = visit(node.getParameters().get(1));
            log.warn("Can't analyze output of array creation");
            return Complexity.runtime(size.output);
        default:
            throw new UnsupportedOperationException("Unsupported special call: " + node.getMethodName());
        }
    }

    @Override
    public Complexity visitCall(Call node) {
        final String methodName = node.getMethodName();
        final FunctionCall method = knownMethods.get(methodName);

        List<Complexity> argumentComplexities = node.getArguments().stream().map(this::visit).collect(Collectors.toList());
        MathExpression argumentTime;
        if (node.getArguments().isEmpty()) {
            argumentTime = Constant.ZERO;
        } else {
            argumentTime = new Addition(argumentComplexities.stream().map(c -> c.runtime).collect(Collectors.toList()));
        }

        List<MathExpression> arguments = argumentComplexities.stream().map(c -> c.output).collect(Collectors.toList());

        if (methodName.equals(currentMethodName)) {
            log.info("Recursive method!");
            return Complexity.of(
                    new Addition(argumentTime, new Function(methodName, arguments)),
                    new Function(methodName, arguments));
        }

        Preconditions.checkNotNull(method, "Unrecognized function: " + methodName);

        MathExpression runtime = new Addition(argumentTime, method.getRuntime(arguments));
        MathExpression output = method.getOutput(arguments);

        return Complexity.of(runtime, output);
    }

    @Override
    public Complexity visitBinOp(BinOp node) {
        Complexity left = visit(node.getLeft());
        Complexity right = visit(node.getRight());
        MathExpression runtime = new Addition(left.runtime, right.runtime);
        MathExpression output = null;
        if (left.output != null && right.output != null) {
            switch (node.getOperator()) {
            case "+":
                output = new Addition(left.output, right.output);
                break;
            case "-":
                output = new Subtraction(left.output, right.output);
                break;
            case "*":
                output = new Multiplication(left.output, right.output);
                break;
            case "/":
                output = new Division(left.output, right.output);
                break;
            default:
                log.warn("Can't analyze output of operator: " + node.getOperator());
            }
        }
        return Complexity.of(runtime, output);
    }

    @Override
    public Complexity visitUnaryOp(UnaryOp node) {
        Complexity value = visit(node.getValue());
        MathExpression runtime = value.runtime;
        MathExpression output = null;
        if (value.output != null) {
            switch (node.getOperator()) {
            case "+":
                output = value.output;
                break;
            case "-":
                output = new Negation(value.output);
                break;
            default:
                log.warn("Can't analyze output of operator: " + node.getOperator());
            }
        }
        return Complexity.of(runtime, output);
    }

    @Override
    public Complexity visitLiteral(Literal node) {
        try {
            int value = Integer.parseInt(node.getText());
            return Complexity.of(Constant.ONE, new Constant(value));
        } catch (NumberFormatException ex) {
            log.warn("Can't analyze output of literal: " + node.getText());
            return Complexity.runtime(Constant.ONE);
        }
    }

    @Override
    public Complexity visitType(Type node) {
        throw new UnsupportedOperationException("Variable type has no time or output complexity");
    }

    @Override
    public Complexity visitMultipleAstNodes(MultipleAstNodes node) {
        throw new IllegalStateException("MultipleAstNodes should not be in the final AST");
    }

}
