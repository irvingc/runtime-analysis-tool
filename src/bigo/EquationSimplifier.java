package bigo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;

import ast.math.Addition;
import ast.math.Constant;
import ast.math.Division;
import ast.math.Function;
import ast.math.Log;
import ast.math.MathExpression;
import ast.math.MathExpressionVisitor;
import ast.math.MultiTerm;
import ast.math.Multiplication;
import ast.math.Negation;
import ast.math.Recurrence;
import ast.math.Subtraction;
import ast.math.Sum;
import ast.math.Variable;

public class EquationSimplifier implements MathExpressionVisitor<MathExpression> {
    private List<Constant> collectConstants(List<MathExpression> expressions) {
        return expressions.stream()
                .filter(e -> e instanceof Constant)
                .map(e -> (Constant) e)
                .collect(Collectors.toList());
    }

    private List<MathExpression> collectNonConstants(List<MathExpression> expressions) {
        return expressions.stream()
                .filter(e -> !(e instanceof Constant))
                .collect(Collectors.toList());
    }

    private List<MathExpression> simplify(List<MathExpression> expressions) {
        return expressions.stream().map(this::visit).collect(Collectors.toList());
    }

    private List<MathExpression> foldUp(Class<? extends MultiTerm> target, List<MathExpression> exprs) {
        List<MathExpression> output = new ArrayList<>();
        for (MathExpression expr : exprs) {
            if (expr.getClass().equals(target)) {
                output.addAll(target.cast(expr).getTerms());
            } else {
                output.add(expr);
            }
        }
        return output;
    }

    private Constant mergeConstants(List<Constant> constants, BinaryOperator<Integer> op, int identity) {
        return new Constant(constants.stream().map(Constant::getValue).reduce(identity, op));
    }

    private List<MathExpression> collapse(Class<? extends MultiTerm> collapseClass, List<MathExpression> expr,
            BinaryOperator<Integer> op, int identity) {
        List<MathExpression> terms = this.simplify(expr);
        terms = this.foldUp(collapseClass, terms);
        List<MathExpression> nonConstants = this.collectNonConstants(terms);
        List<Constant> constants = this.collectConstants(terms);
        if (!constants.isEmpty()) {
            Constant constant = this.mergeConstants(constants, op, identity);
            if (constant.value != identity) {
                nonConstants.add(constant);
            }
        }
        return nonConstants;
    }

    private <T extends MultiTerm> MathExpression bust(T node) {
        if (node.getTerms().size() == 1) {
            return node.getTerms().get(0);
        } else {
            return node;
        }
    }

    @Override
    public MathExpression visitAddition(Addition expr) {
        return this.bust(new Addition(this.collapse(Addition.class, expr.getTerms(), (a, b) -> a + b, 0)));
    }

    @Override
    public MathExpression visitConstant(Constant expr) {
        return expr;
    }

    @Override
    public MathExpression visitDivision(Division expr) {
        return new Division(this.simplify(expr.getTerms()));
    }

    @Override
    public MathExpression visitLog(Log expr) {
        return new Log(this.visit(expr.getExpression()));
    }

    @Override
    public MathExpression visitMultiplication(Multiplication expr) {
        return this.bust(new Multiplication(this.collapse(Multiplication.class, expr.getTerms(), (a, b) -> a * b, 1)));
    }

    @Override
    public MathExpression visitNegation(Negation expr) {
        return new Negation(this.visit(expr.getOriginalValue()));
    }

    @Override
    public MathExpression visitSubtraction(Subtraction expr) {
        return new Subtraction(simplify(expr.getTerms()));
    }

    @Override
    public MathExpression visitVariable(Variable expr) {
        return expr;
    }

    @Override
    public MathExpression visitSum(Sum expr) {
        MathExpression start = this.visit(expr.getStart());
        MathExpression end = this.visit(expr.getEnd());
        MathExpression body = this.visit(expr.getBody());

        if (body.nodeName().equals("Constant") && start.nodeName().equals("Constant")) {
            return this.visit(new Multiplication(Arrays.asList(
                    new Subtraction(Arrays.asList(end, start)),
                    body)));
        } else {
            return new Sum(start, end, body, expr.getIndex());
        }
    }

    @Override
    public MathExpression visitRecurrence(Recurrence expr) {
        return new Recurrence(
                expr.function,
                expr.baseConditions,
                this.visit(expr.recursiveCase),
                this.visit(expr.baseCase));
    }

    public static MathExpression simplify(MathExpression expr) {
        if (expr == null) return null;
        return new EquationSimplifier().visit(expr);
    }

    @Override
    public MathExpression visitFunction(Function expr) {
        return new Function(expr.name, simplify(expr.arguments));
    }

}
