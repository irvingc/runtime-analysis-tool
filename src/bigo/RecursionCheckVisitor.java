package bigo;

import java.util.List;

import ast.simple.Assignment;
import ast.simple.AstNode;
import ast.simple.AstNodeVisitor;
import ast.simple.BinOp;
import ast.simple.Call;
import ast.simple.ClassDecl;
import ast.simple.ForEachLoop;
import ast.simple.ForLoop;
import ast.simple.IfElse;
import ast.simple.Literal;
import ast.simple.Lookup;
import ast.simple.MethodDecl;
import ast.simple.MultipleAstNodes;
import ast.simple.Parameter;
import ast.simple.Return;
import ast.simple.SpecialCall;
import ast.simple.Type;
import ast.simple.UnaryOp;
import ast.simple.VariableDecl;

public class RecursionCheckVisitor extends AstNodeVisitor<Boolean> {

    private String methodName;

    @Override
    public Boolean visitClassDecl(ClassDecl node) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Boolean visitMethodDecl(MethodDecl node) {
        methodName = node.getName();
        return visitMultipleAstNodes(node.getBody());
    }

    @Override
    public Boolean visitParameter(Parameter node) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Boolean visitVariableDecl(VariableDecl node) {
        return false;
    }

    @Override
    public Boolean visitAssignment(Assignment node) {
        return visit(node.getValue());
    }

    @Override
    public Boolean visitLookup(Lookup node) {
        return false;
    }

    @Override
    public Boolean visitForEachLoop(ForEachLoop node) {
        return visit(node.getSequence()) || visitMultipleAstNodes(node.getBody());
    }

    @Override
    public Boolean visitForLoop(ForLoop node) {
        return visit(node.getCounter()) || visit(node.getEnd()) || visit(node.getChange()) || visitMultipleAstNodes(node.getBody());
    }

    @Override
    public Boolean visitReturn(Return node) {
        return visit(node.getValue());
    }

    @Override
    public Boolean visitIfElse(IfElse node) {
        return visit(node.getCondition()) || visitMultipleAstNodes(node.getTrueBranch()) || visitMultipleAstNodes(node.getFalseBranch());
    }

    @Override
    public Boolean visitSpecialCall(SpecialCall node) {
        return visit(node.getObject()) || node.getParameters().stream().anyMatch(this::visit);
    }

    @Override
    public Boolean visitCall(Call node) {
        return methodName.equals(node.getMethodName()) || visitMultipleAstNodes(node.getArguments());
    }

    @Override
    public Boolean visitBinOp(BinOp node) {
        return visit(node.getLeft()) || visit(node.getRight());
    }

    @Override
    public Boolean visitUnaryOp(UnaryOp node) {
        return visit(node.getValue());
    }

    @Override
    public Boolean visitLiteral(Literal node) {
        return false;
    }

    @Override
    public Boolean visitType(Type node) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Boolean visitMultipleAstNodes(MultipleAstNodes node) {
        throw new UnsupportedOperationException();
    }

    private boolean visitMultipleAstNodes(List<AstNode> nodes) {
        return nodes.stream().anyMatch(this::visit);
    }

    public static boolean isRecursive(MethodDecl method) {
        return new RecursionCheckVisitor().visit(method);
    }

}
