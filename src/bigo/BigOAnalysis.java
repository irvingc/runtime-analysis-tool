package bigo;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.logging.LogFactory;

import com.google.common.collect.ImmutableMap;

import ast.math.Constant;
import ast.math.MathExpression;
import ast.math.Variable;
import ast.simple.ClassDecl;
import ast.simple.MethodDecl;

public class BigOAnalysis {

    private static final org.apache.commons.logging.Log log = LogFactory.getLog(BigOAnalysis.class);

    private final ImmutableMap<String, FunctionCall> methodComplexities;

    private BigOAnalysis(Map<String, FunctionCall> methodComplexities) {
        this.methodComplexities = ImmutableMap.copyOf(methodComplexities);
    }

    public MathExpression getRuntimeComplexity(String methodName) {
        return methodComplexities.get(methodName).complexity.runtime;
    }

    public MathExpression getOutputComplexity(String methodName) {
        return methodComplexities.get(methodName).complexity.output;
    }

    public static BigOAnalysis of(ClassDecl clazz) {
        Map<String, FunctionCall> methodComplexities = new HashMap<>();

        methodComplexities.put("System.out.println", new FunctionCall(
                Collections.singletonList(new Variable("s")),
                Complexity.runtime(Constant.ONE)));

        methodComplexities.put("list.get", new FunctionCall(
                Collections.singletonList(new Variable("s")),
                Complexity.runtime(Constant.ONE)));

        methodComplexities.put("list.size", new FunctionCall(
                Collections.emptyList(),
                Complexity.of(Constant.ONE, new Variable("list.size"))));

        for (MethodDecl method : clazz.methods) {
//            log.info("Analyzing: " + method.getName());
            MethodComplexityVisitor visitor = new MethodComplexityVisitor(methodComplexities);
            Complexity complexity = visitor.visit(method);
            List<Variable> parameters = method.getParameters().stream().map(p -> new Variable(p.getName())).collect(Collectors.toList());
            Complexity simple = Complexity.of(
                    EquationSimplifier.simplify(complexity.runtime),
                    EquationSimplifier.simplify(complexity.output));
            FunctionCall call = new FunctionCall(parameters, simple);
            methodComplexities.put(method.getName(), call);
        }

        return new BigOAnalysis(methodComplexities);
    }

}
