package bigo;

import java.util.StringJoiner;
import java.util.stream.Collectors;

import ast.math.Addition;
import ast.math.Constant;
import ast.math.Division;
import ast.math.Function;
import ast.math.Log;
import ast.math.MathExpression;
import ast.math.MathExpressionVisitor;
import ast.math.Multiplication;
import ast.math.Negation;
import ast.math.Recurrence;
import ast.math.Subtraction;
import ast.math.Sum;
import ast.math.Variable;

public class EquationDisplayPrinter implements MathExpressionVisitor<String> {

    public static final EquationDisplayPrinter INSTANCE = new EquationDisplayPrinter();

    private String removeExtraParens(String eq) {
        while (eq.startsWith("(") && eq.endsWith(")")) {
            eq = eq.substring(1, eq.length() - 1);
        }
        return eq;
    }

    @Override
    public String visitAddition(Addition expr) {
        StringJoiner output = new StringJoiner(" + ", "(", ")");
        for (MathExpression e : expr.getTerms()) {
            output.add(this.visit(e));
        }
        return output.toString();
    }

    @Override
    public String visitConstant(Constant expr) {
        return "" + expr.getValue();
    }

    @Override
    public String visitDivision(Division expr) {
        StringJoiner output = new StringJoiner("/");
        for (MathExpression e : expr.getTerms()) {
            output.add(this.visit(e));
        }
        return output.toString();
    }

    @Override
    public String visitLog(Log expr) {
        return "log(" + this.visit(expr) + ")";
    }

    @Override
    public String visitMultiplication(Multiplication expr) {
        StringJoiner output = new StringJoiner(" * ", "(", ")");
        for (MathExpression e : expr.getTerms()) {
            output.add(this.visit(e));
        }
        return output.toString();
    }

    @Override
    public String visitNegation(Negation expr) {
        return "-" + this.visit(expr.getValue());
    }

    @Override
    public String visitSubtraction(Subtraction expr) {
        StringJoiner output = new StringJoiner("-", "(", ")");
        for (MathExpression e : expr.getTerms()) {
            output.add(this.visit(e));
        }
        return output.toString();
    }

    @Override
    public String visitVariable(Variable expr) {
        return expr.getName();
    }

    @Override
    public String visitSum(Sum expr) {
        return String.format("(sum_(%s=%s)^(%s) %s)",
                this.visit(expr.getIndex()),
                removeExtraParens(this.visit(expr.getStart())),
                removeExtraParens(this.visit(expr.getEnd())),
                this.visit(expr.getBody()));
    }

    @Override
    public String visitRecurrence(Recurrence expression) {
        StringJoiner paramJoiner = new StringJoiner(", ", "(", ")");
        for (Variable var : expression.getParameters()) {
            paramJoiner.add(var.name);
        }

        StringJoiner zeroJoiner = new StringJoiner(", ", "(", ")");
        for (Variable var : expression.getParameters()) {
            zeroJoiner.add(String.valueOf(expression.baseConditions.get(var).getValue()));
        }

        return String.format("%s%s = %s, %s%s = %s",
                expression.getFunction().getName(),
                paramJoiner.toString(),
                this.visit(expression.getRecursiveCase()),
                this.visit(expression.getFunction()),
                zeroJoiner.toString(),
                this.visit(expression.getBaseCase()));
    }

    public static String print(MathExpression expr) {
        return INSTANCE.visit(expr);
    }

    @Override
    public String visitFunction(Function expr) {
        return String.format("%s(%s)", expr.name, String.join(", ", expr.arguments.stream().map(arg -> visit(arg)).collect(Collectors.toList())));
    }

}
