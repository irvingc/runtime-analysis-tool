package bigo;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

import ast.math.MathExpression;
import ast.math.Variable;

public class FunctionCall {

    // Ordered list of formal parameter bindings
    public final ImmutableList<Variable> parameters;

    public final Complexity complexity;

    public FunctionCall(List<Variable> parameters, Complexity complexity) {
        this.parameters = ImmutableList.copyOf(parameters);
        this.complexity = complexity;
    }

    public MathExpression getRuntime(List<MathExpression> arguments) {
        return complexity.runtime.bindVariables(bindArguments(arguments));
    }

    public MathExpression getRuntime(MathExpression ... arguments) {
        return getRuntime(Arrays.asList(arguments));
    }

    public MathExpression getOutput(List<MathExpression> arguments) {
        if (complexity.output == null) return null;
        return complexity.output.bindVariables(bindArguments(arguments));
    }

    public MathExpression getOutput(MathExpression ... arguments) {
        return getOutput(Arrays.asList(arguments));
    }

    private Map<Variable, MathExpression> bindArguments(List<MathExpression> arguments) {
        Preconditions.checkArgument(arguments.size() <= parameters.size());
        Map<Variable, MathExpression> bindings = new HashMap<>();
        for (int i = 0; i < arguments.size(); i++) {
            bindings.put(parameters.get(i), arguments.get(i));
        }
        return bindings;
    }

}
