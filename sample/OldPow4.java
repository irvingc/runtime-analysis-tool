public class OldPow4 {

    int square(int n) {
        return n * n;
    }

    int pow4(int n) {
        return square(n) * square(n);
    }

}
