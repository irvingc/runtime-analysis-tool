public class NewPow4 {

    int square(int n) {
        return n * n;
    }

    int pow4(int n) {
        int sum = 0;
        for (int i = 0; i < square(n); i++) {
            sum += square(n);
        }
        return sum;
    }

}
