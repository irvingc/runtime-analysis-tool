public class NewArraySum {
    public int sum(List<Integer> list) {
        int s = 0;
        for (int i = 0; i < list.size(); i++) {
            s += list.get(i);
        }
        return s;
    }
}
